package com.subbirhosain.firebasepushexample.helper;

/**
 * Created by SubbirHosain on 2/3/2018.
 */

public class Constants {

    public static final String CHANNEL_ID = "mychannelid";
    public static final String CHANNEL_NAME = "mychannelname";
    public static final String CHANNEL_DESCRIPTION = "my description";


}

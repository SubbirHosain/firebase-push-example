package com.subbirhosain.firebasepushexample.helper;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.subbirhosain.firebasepushexample.MainActivity;
import com.subbirhosain.firebasepushexample.R;

/**
 * Created by SubbirHosain on 2/3/2018.
 */

public class MyNotificationManager {

    private Context mCtx;
    private static MyNotificationManager mInstance;

    private MyNotificationManager(Context mCtx) {
        this.mCtx = mCtx;
    }

    //returns instance singleton pattern instance
    public static synchronized MyNotificationManager getInstance(Context context){
        if (mInstance==null){
            mInstance = new MyNotificationManager(context);
        }
        return mInstance;
    }

    //display notification
    public void displayNotification(String title,String body){

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx,Constants.CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body);

        //display the notification with intent
        Intent intent = new Intent(mCtx, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(mCtx,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        //NOW SET THE PENDING INTENT TO THE NOTIFICATION BUILDER
        mBuilder.setContentIntent(pendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);

        if (mNotificationManager!=null){
            mNotificationManager.notify(0,mBuilder.build());
        }
    }


}

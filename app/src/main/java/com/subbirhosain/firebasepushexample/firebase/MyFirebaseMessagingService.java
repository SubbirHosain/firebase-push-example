package com.subbirhosain.firebasepushexample.firebase;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.subbirhosain.firebasepushexample.helper.MyNotificationManager;

/**
 * Created by SubbirHosain on 2/3/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String title = remoteMessage.getNotification().getTitle();
        String body = remoteMessage.getNotification().getBody();

        //Build the notification
        MyNotificationManager.getInstance(getApplicationContext())
                .displayNotification(title,body);

    }
}
